import { useState, useEffect } from "react";
import { projectFirestore } from "../firebase/config.ts";
import { getDatabase, ref, set } from "firebase/database";

// get record...
export const useFirestore = async (collection:string) => {

  const docs:any[] = [];
  console.log('firebaseStart');

  async function unsub() {
    const dbref = projectFirestore
    .collection(collection);
    try {
      const snapshots = await dbref.get();
      snapshots.forEach((doc) => {
        docs.push(doc.data()); 
      });
    } catch (err)
    {
      console.log(err);
    }
  }
  await unsub();
  console.log("GBear: query result:", docs);
  return {docs};
}

export const useSetUnitFirestore = async (collection:string, record:object ) => {
  let batch = projectFirestore.batch();
  console.log('setFireStore => : ', record);
  const unsub = async () => {
    var docRef = projectFirestore.collection(collection).doc(); //automatically generate unique id
    batch.set(docRef, record);
    await batch.commit();
    }
  console.log('write completed');
  await unsub();
}

// batch set record...
export const useSetFirestore = async (collection: string, record: Array<object>) => {
    let batch = projectFirestore.batch();
    var index: number = record.length;
    console.log('setFireStore => : ', index, record);
    const unsub = async () => {
      for (let i:number = 0; i < index; i++) {
        var docRef = projectFirestore.collection(collection).doc(); //automatically generate unique id
        batch.set(docRef, record[i]);
        // YOUR UPDATES
        if ((i + 1) % 499 === 0) {
          await batch.commit();
          batch = projectFirestore.batch();
          console.log('setFireStore => : ', i / 500);
        }
      }
      // For committing final batch
      if (((index % 499) == 0) === false) {
        await batch.commit();
        console.log('setFireStore => : end');
      }
      console.log('write completed');
    }
    await unsub();
}

// batch set record...
export const useUpdateFirestore =  async (collection: string, record: Array<object>) => {
  let collectionRef = projectFirestore.collection(collection);
  const unsub = async () => {
    try {
      let batch = projectFirestore.batch();
      const documentSnapshotArray = await collectionRef.get(); // here you can add condition...like this => .where('uid', '==', before.uid)
      const records = documentSnapshotArray.docs;
      const index: number = documentSnapshotArray.size;
        for (let i:number = 0; i < index; i++) {
          const docRef = records[i].ref;
          // YOUR UPDATES
          batch.update(docRef, {isDeleted: false}); // record[i].mark or content...
          if ((i + 1) % 499 === 0) {
            await batch.commit();
            batch = projectFirestore.batch();
          }
        }
        // For committing final batch
        if (((index % 499) == 0) === false) {
          await batch.commit();
        }
        console.log('update completed');
    } catch (error)
    {
      console.error(`updateWorkers() errored out : ${error.stack}`);
    }
  }
  await unsub();
}
